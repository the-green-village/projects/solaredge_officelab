import pandas as pd
import json
from tgvfunctions import tgvfunctions

data = pd.read_json('../../ram_drive/batch.json', lines=True)

# Read in the translation key
translation = pd.read_json("translation.json")

## Get the scaling factors, and decide which columns have a corresponding scaling factor
SFS = {col: data.iloc[0][col] for col in data.columns if "_SF" in col}
other_cols = [col for col in data.columns if "_SF" not in col]

scaled = {}
for col in other_cols:
    for key in SFS.keys():
        if key[:-3] in col:
            scaled[col] = key

## Yet to use: SunspecID, Sunspec_DID, Sunspec_length, C_DeviceAddress,
## And manufacturer error codes I_Status 4
## Some values that are not scaled, are measurement values such as;
## Sunspec_ID, SunSpec_DID, SunSpec_Length
row = data.iloc[0]
value = {
        "project_id": "officelab-climate",
        "application_id": str(row["C_SunSpec_ID"]),
        "device_id": str(row["C_Model"]) + " " + str(row["C_Version"]),
        "project_description": "Solaredge Inverter Office Lab",
        "application_description": "Solaredge SunSpec Inverter",
        "device_description": "Three Phase Inverter",
        "device_manufacturer": row["C_Manufacturer"],
        "device_type": row["NM_Module"],
        "device_serial": row["C_SerialNumber"]}
metadata_cols = ["C_SunSpec_ID", "C_Model", "C_Version", "C_Manufacturer", "NM_Module", "C_SerialNumber", "Time"]

for col in metadata_cols:
    other_cols.remove(col)
producer = tgvfunctions.makeproducer("officelab-solaredge-producer")

for idx, row in data.iterrows():
    measurements = []
    value["timestamp"] = row["Time"]
    for col in other_cols:
        v = row[col]
        if col in scaled.keys():
            SF = data.iloc[idx][scaled[col]]
            v = row[col] * 10.0 **(SF)
        if not pd.isnull(translation[col]["unit"]):
            measurement = {"measurement_id": col,
                           "measurement_description": translation[col]["measurement_description"],
                           "value": v,
                           "unit": translation[col]["unit"]
                           }
            measurements.append(measurement)
    value["measurements"] = measurements
    tgvfunctions.produce_fast(producer, value)
producer.flush()
